local was_crosshair_visible = {}            -- KEY: p_name; VALUE = true/false

function weapons_lib.HUD_crosshair_create(p_name)
  Panel:new("wl_crosshair", {
    player = p_name,
    bg = "",
    bg_scale = {x = 2, y = 2},
    position = { x = 0.5, y = 0.5 },
    alignment = {x = 0, y = 0},
    visible = false
  })
end



function weapons_lib.HUD_crosshair_update(p_name, w_name, turn_red)
  local player = minetest.get_player_by_name(p_name)
  local panel = panel_lib.get_panel(p_name, "wl_crosshair")
  local weap = minetest.registered_nodes[w_name] or minetest.registered_tools[w_name]
  local is_wielded_item_weapon = weapons_lib.is_weapon(player:get_wielded_item():get_name())

  -- Definiscio la texture:
  -- 1. se l'oggetto in mano è un'arma, devo cambiare texture
  -- 2. se turn_red è vero o se è nil ed era precedentemente rosso, col è rosso

  local prev_txt = panel:get_info().bg.text
  local was_red = string.find(prev_txt, "%^%[multiply")
  local txtr = ""
  local col = (turn_red or (turn_red == nil and was_red)) and "^[multiply:#ff492c" or ""

  if is_wielded_item_weapon then
    txtr = weap.crosshair
  else
    txtr = was_red and prev_txt:sub(1,-19) or prev_txt
  end

  panel:update({bg = txtr .. col})

  -- Definisco la visibilità
  if not is_wielded_item_weapon then
    if panel:is_visible() then
      panel:hide()

      if was_crosshair_visible[p_name] then
        player:hud_set_flags({crosshair = true})
      end
    end

  else
    if not panel:is_visible() then
      was_crosshair_visible[p_name] = player:hud_get_flags().crosshair

      panel:show()
      player:hud_set_flags({crosshair = false}) -- nascondere il mirino di base sempre e comunque, lasciarli entrambi non ha senso
    end
  end
end
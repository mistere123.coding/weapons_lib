minetest.register_on_joinplayer(function(player)
  local p_name = player:get_player_name()

  weapons_lib.init_player(p_name)
  weapons_lib.HUD_crosshair_create(p_name)

  player:get_meta():set_int("wl_weapon_state", 0)
end)



minetest.register_on_dieplayer(function(player, reason)
  local i_name = player:get_wielded_item():get_name()
  if weapons_lib.is_weapon(i_name) then
    weapons_lib.deactivate_zoom(player, minetest.registered_nodes[i_name])
  end
end)
local dmg1 = 3
local dmg2 = 5
local ammo2 = 3

weapons_lib.register_weapon("weapons_lib:test1", {

  description = "Test weapon #1: raycast\n\nInfinite magazine\nCannot be used if HP <= 10",

  wield_scale = {x=1.34, y=1.34, z=1.34},
  wield_image = "test1_sprite.png",
  inventory_image = "test1_sprite.png",

  weapon_type = "ranged",
  magazine = -1,
  reload_time = 2,
  sound_reload = { track = "test1_reload", description = "Weapon reloads" },
  crosshair = "test1_crosshair.png",

  can_use_weapon = function(player, action)
    return player:get_hp() > 10
  end,

  action1 = {
    type = "raycast",
    description = "raycast - decrease damage with distance (" .. dmg1 .. ")",
    damage = dmg1,
    range = 30,
    delay = 0.1,
    --fire_spread = 0.2,

    decrease_damage_with_distance = true,
    continuous_fire = true,

    sound = { track = "test1_attack", description = "Weapon shoots" },
    trail = {
      image = "test1_trail1.png",
      size = 2,
      amount = 5
    },
  },

  action2 = {
    type = "raycast",
    description = "raycast - slower steadier shot with less reach (" .. dmg2 .. "); consumes " .. ammo2 .. " bullets each",
    damage = dmg2,
    range = 20,
    delay = 0.5,
    ammo_per_use = ammo2,
    --TODO: booleano per far critici o meno?

    continuous_fire = true,

    sound = { track = "test1_attack", description = "Weapon shoots" },
    trail = {
      image = "test1_trail2.png",
      size = 2,
      amount = 5
    },
  }
})

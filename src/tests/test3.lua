weapons_lib.register_weapon("weapons_lib:test3", {

  description = "Test weapon #3: entity-type bullets",

  wield_scale = {x=1.3, y=1.3, z=1.3},
  wield_image = "test3_sprite.png",
  inventory_image = "test3_sprite.png",

  weapon_type = "ranged",
  magazine = 5,
  reload_time = 1,
  sound_reload = { track = "test1_reload", description = "Weapon reloads" },
  crosshair = "test1_crosshair.png",

  action1 = {
    type = "bullet",
    description = "bullet - damage 10",
    damage = 10,
    delay = 0.7,

    continuous_fire = true,
    -- TODO: range non ha senso, c'è lifetime sul proiettile
    -- TODO: per pierce, bisogna disattivare collisioni giocator3 (nodi possono rimanere (TODO: vari tipi di pierce es x armi che attraversano muri?))
    -- e controllare raggio attorno a proiettile, sennò non c'è modo di fargli attraversare l'entità/giocatorə

    sound = { track = "test1_attack", description = "Weapon shoots" },
    -- TODO: trail direttamente nel proiettile o mantengo coerenza con armi generiche?
    -- `damage` x es. ora lo prende da qua, non da proiettile; eviterei forse 1 e 1

    bullet = {
      name = "weapons_lib:bullet_test", -- TODO: ha senso impostare il nome?

      mesh = "test_bullet.obj",
      textures = {"test1_trail2.png"},
      collisionbox = {-0.1, -0.1, -0.1, 0.1, 0.1, 0.1},

      speed = 30,
      --bounciness = 0, -- TODO: di base 0. Utile per esempio per granate o proiettili che rimbalzano contro ai muri tipo crylink Xonotic
      lifetime = 3,

      explosion = { -- TODO: si può portare fuori, x tutte le armi (es. Cannonade); motivo per cui remove_on_contact rimane fuori, solo x proiettili
        range = 4,
        damage = 10,
        texture = "test1_trail2.png",
      },

      remove_on_contact = true, -- TODO: considera rinominare in remove_on_contact (l'esplosione avviene se c'è explosion)
      gravity = false,

      -- TODO particelle per quando svanisce? Tipo effetto fumo

      trail = {
        image = "test1_trail2.png",
        life = 1,
        size = 2,
        interval = 5, -- in step
        amount = 20,
      }
    },
  },
})

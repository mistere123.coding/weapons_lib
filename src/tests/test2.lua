local dmg1      = 3
local dmg1hold  = 6.8
local dmg1air   = 3.7
local dmg2      = 7

weapons_lib.register_weapon("weapons_lib:test2", {

  description = "Test weapon #2: melee\n\nBig knockback",

  wield_scale = {x=1.3, y=1.3, z=1.3},
  wield_image = "test2_sprite.png",
  inventory_image = "test2_sprite.png",
  crosshair = "test2_crosshair.png",

  weapon_type = "melee",
  range = 5,

  --[[action1 = {
    type = "punch",
    description = S("slash, @1♥", "<style color=#f66c77>" .. dmg1),
    damage = dmg1,
    delay = 0.4,
    sound = "bl_sword_hit",
  },]]

  -- TEMP: questa dovrebbe diventare action1_hold una volta che:
  --   1. Sarà possibile personalizzare l'animazione dell'oggetto tenuto in mano.
  --   Vedasi https://github.com/minetest/minetest/issues/2811.
  --   2. Sarà possibile ritardare l'azione del clic.
  --   Vedasi https://github.com/minetest/minetest/issues/13733
  action1 = {
    type = "punch",
    description = "punch - push enemies",
    damage = dmg1hold,
    knockback = 40,
    delay = 1,
    sound = { track = "test2_attack", description = "Weapon swings" }
  },

  action2 = {
    type = "custom",
    description = "custom - dash forward",
    damage = dmg2,
    delay = 2.5,
    physics_override = { speed = 0.5, jump = 0 }, -- TODO: rotto, da implementare. Sarebbe buona cosa usare dei moltiplicatori e divisori
    sound = { track = "test2_attack", description = "Weapon swings" },

    on_use = function(player, weapon, action)
      local pointed_objects = weapons_lib.get_pointed_objects(player, 5, true)
      local dir = player:get_look_dir()

      dir.y = 0

      local player_vel = player:get_velocity()
      local sprint = vector.multiply(dir,18)

      player:add_velocity(sprint)
      player_vel = vector.multiply(player_vel, -0.7)
      player:add_velocity(player_vel)

      if not pointed_objects then return end
      weapons_lib.apply_damage(player, pointed_objects, weapon, action)
    end
  }
})
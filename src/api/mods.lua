local default_settings = {
  RELOAD_KEY = "Q",
  SHOOT_SPEED_MULTIPLIER = 0.66
}

function weapons_lib.register_mod(mod, def)
  weapons_lib.mods[mod] = table.copy(default_settings)

  local settings = weapons_lib.mods[mod]

  for k, v in pairs(def) do
    settings[k] = v
  end
end
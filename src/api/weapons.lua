local S = minetest.get_translator("weapons_api")

local function register_action() end
local function calc_action() end
local function wait_for_held_action() end
local function wait_for_charged_action() end
local function can_use_weapon() end
local function set_attack_stance() end
local function slow_down() end
local function speed_up() end
local function run_action() end
local function attack_loop() end
local function decrease_magazine() end
local function attack_hitscan() end
local function attack_melee() end
local function attack_bullet() end
local function attack_end() end
local function weapon_zoom() end
local function weapon_reload() end
local function draw_particles() end

-- ogni volta che un'arma spara, se il suo ritardo è minore di 0.5s, viene eseguita
-- una funzione dopo 0.5s. Tuttavia, se si spara con un arma con ritardo minore e
-- subito dopo con un'altra (prima dei fatidici 0.5s), quella funzione da 0.5s va
-- annullata. Ne tengo traccia qui
local slow_down_func = {}       -- KEY: p_name; VALUE: timer func
-- TODO: considera se cambiare nome, tipo `data` o boh, così da usare p_data come variabile locale nelle varie funzioni
local p_data = {}               -- KEY: p_name; VALUE: {current_weapon, magazine = {w_name1 = amount, w_name2 = amount}}





----------------------------------------------
--------------INTERNAL USE ONLY---------------
----------------------------------------------

function weapons_lib.init_player(p_name)
  if not p_data[p_name] then
    p_data[p_name] = {magazine = {}}

    -- TEMP: serve https://github.com/minetest/minetest/issues/13477 per togliere controllo in attack_end.
    -- Per il momento uso questo valore che registro prima che spari
    p_data[p_name].speed_before_shooting = 0

    -- TODO funzione che restituisca tutte le armi registrate, per qui riempirle coi proiettili (solo se han proiettili)
  end
end

----------------------------------------------

--v---------------- globalstep -------------------v--
-- si attiva ogniqualvolta viene cambiato l'oggetto in mano
minetest.register_globalstep(function(dtime)
  for _, pl_stats in pairs(minetest.get_connected_players()) do
    local pl_name = pl_stats:get_player_name()
    local pl_data = p_data[pl_name]
    local player = minetest.get_player_by_name(pl_name)
    local pl_meta = player:get_meta()
    local weap_state = pl_meta:get_int("wl_weapon_state")
    local wld_item = player:get_wielded_item()
    local w_name = wld_item:get_name()
    local is_weapon = weapons_lib.is_weapon(w_name)
    local curr_weap = pl_data and pl_data.current_weapon

    -- se aveva un'arma
    if curr_weap then
      if w_name ~= curr_weap then
        -- disattivo zoom
        if pl_meta:get_int("wl_zooming") == 1 then
          weapons_lib.deactivate_zoom(player, minetest.registered_nodes[curr_weap])
        end

        -- se stava caricando, sparando, o si stava parando, annulla l'azione
        if weap_state == 1 or weap_state == 2 or weap_state == 5 then
          pl_meta:set_int("wl_weapon_state", 0)
        end

        pl_data.current_weapon = is_weapon and w_name or nil

        if is_weapon and not pl_data.magazine[w_name] then
          pl_data.magazine[w_name] = minetest.registered_nodes[w_name].magazine
        end

        weapons_lib.HUD_crosshair_update(pl_name, w_name)
      end

    -- se non la aveva
    else
      if is_weapon then
        pl_data.current_weapon = w_name

        if not pl_data.magazine[w_name] then
          pl_data.magazine[w_name] = minetest.registered_nodes[w_name].magazine -- TODO: verifica che non son sicuro di 'sta cosa; prob meglio inizializzare per ogni arma, riempiendo tabella
        end

        -- se un'arma stava caricando, il mirino mostralo rosso
        weapons_lib.HUD_crosshair_update(pl_name, w_name)
      end
    end
  end
end)
--^---------------- globalstep -------------------^--



-- TODO: sarebbe bello usare full_punch_interval per segnalare tempo di ricarica, ma non si possono avere tempi di ricarica differenti su singolo oggetto
function weapons_lib.register_weapon(name, def)
  -- usato per avere una dichiarazione pulita E al tempo stesso non dover passare
  -- anche il nome in on_use (che lo richiede)
  def.name = name
  def.mod = def.name:sub(1, string.find(def.name, ":") -1)

  if def.sound_reload then
    local sound_reload = def.sound_reload
    audio_lib.register_sound("sfx", sound_reload.track, sound_reload.description, sound_reload.params)
  end

  local groups

  --[[ TODO che fare?
  -- specifica il gruppo per capire come renderizzare l'arma in 3D
  if def.mesh then
    groups = {bl_weapon_mesh = 1}
  elseif def.weapon_type == "melee" then
    groups = {bl_sword = 1}
  else
    groups = {bl_weapon = 1}
  end]]

  minetest.register_node(name, {
    mod = def.mod,
    name = def.name,
    groups = groups,

    description = def.description,
    profile_description = def.profile_description or "",

    drawtype = def.mesh and "mesh" or "item",
    mesh = def.mesh or nil,
    tiles = def.tiles or nil,
    wield_image = def.wield_image or nil,
    wield_scale = def.wield_scale,
    inventory_image = def.inventory_image,
    crosshair = def.crosshair,
    use_texture_alpha = def.mesh and "clip" or nil,

    weapon_type   = def.weapon_type,
    action1       = register_action(name, def.action1, "LMB"),    -- TODO: sound tabella, per passare parametri vari. sound = {file, description, params}
    action1_hold  = register_action(name, def.action1_hold, "LMB"),
    action1_air   = register_action(name, def.action1_air, "LMB"),
    action2       = register_action(name, def.action2, "RMB"),
    action2_hold  = register_action(name, def.action2_hold, "RMB"),
    action2_air   = register_action(name, def.action2_air, "RMB"),

    magazine = def.magazine,
    reload_time = def.reload_time,
    sound_reload = def.sound_reload,

    _slow_down_user = def.slow_down_user, -- TODO in futuro si potrebbe mettere impostazione SHOOT_SPEED_MULTIPLIER direttamente dentro l'arma, trasformando questo in tabella o simile

    range = def.weapon_type == "melee" and (def.range or 4) or 0,
    node_placement_prediction = "", -- disable prediction

    _is_weapon_wl = true, -- per is_weapon(

    -- TODO: prob non dovrei usare def in calc_action ma passare la definizione interna dell'oggetto, perché sennò se non è in `def` non lo legge

    -- LMB = first fire
    on_use = function(itemstack, user, pointed_thing)
      calc_action(def, 1, user, pointed_thing)
    end,

    -- RMB = secondary fire
    on_secondary_use = function(itemstack, user, pointed_thing)
      calc_action(def, 2, user, pointed_thing)
    end,

    on_place = function(itemstack, user, pointed_thing)
      calc_action(def, 2, user, pointed_thing)
    end,

    -- Q = reload
    on_drop = function(itemstack, user, pos)
      if weapons_lib.mods[def.mod].RELOAD_KEY == "Q" then
        weapon_reload(user, def)
      else
        return minetest.item_drop(itemstack, user, pos)
      end
    end
  })
end



-- può avere uno o più obiettivi: formato {object = ObjectRef, critical = N.Nx}
function weapons_lib.apply_damage(user, targets, weapon, action)
  local knockback = action.knockback
  local killed_objects = 0
  local tot_damage = 0      -- in caso di più obiettivi colpiti, sommo tutto il danno per poi fare i calcoli alla fine
  local p_name = user:get_player_name()
  local arena = arena_lib.get_arena_by_player(p_name)

  -- TODO richiamo controlli? su BL c'era if not arena or arena.in_queue or arena.in_loading or arena.in_celebration

  if not targets or type(targets) ~= "table" then return end

  if targets.object then
    targets = {targets}
  end

  -- per ogni obiettivo colpito
  for _, target in pairs(targets) do
    local damage = action.damage
    local critical = target.critical
    local target = target.object
    local is_player = target:is_player()
    local t_name = is_player and target:get_player_name() or target:get_luaentity().name

    if target:get_hp() <= 0 then return end
    if is_player and target:get_meta():get_int("wl_immunity") == 1 then return end -- TODO tenere immunità?

    -- TODO altri controlli BL
    -- se giocatorə e obiettivo sono nella stessa squadra, annullo
    --if arena_lib.is_player_in_same_team(arena, p_name, t_name) then return end

    -- eventuale spinta
    if knockback then
      local knk = vector.multiply(user:get_look_dir(), knockback)
      target:add_velocity(knk)
    end

    -- eventuale colpo in testa
    if critical then
      damage = damage * critical
      -- TODO richiamo globale on_critical
      --block_league.HUD_critical_show(p_name)
      --block_league.sound_play("bl_hit_critical", p_name, "not_overlappable")
    end

    -- eventuale danno decrementato a seconda della distanza
    -- TODO: al momento fa crashare con i proiettili perché `range` non esiste
    if action.decrease_damage_with_distance then
      local dist = vector.distance(user:get_pos(), target:get_pos())
      damage = damage - (damage * dist / action.range)
    end

    -- TODO mantenerla o eliminarla?
    -- aggiorno la tabella danni in caso fosse unə giocatorə
    --[[if is_player then
      local dmg_table = arena.players[t_name].dmg_received

      dmg_table[p_name] = {
        timestamp = arena.current_time,
        dmg = arena.current_time > dmg_table[p_name].timestamp - 5 and dmg_table[p_name].dmg + damage or damage,
        weapon = weapon.name
      }
    end]]

    -- applico il danno
    target:punch(user, nil, {damage_groups = {fleshy = damage}})
    tot_damage = tot_damage + damage
  end

  -- calcoli post-danno
  -- TODO BL: su BL è after_damage. Riprodurre suono bl_hit se non morti, sennò block_league.kill
  --on_damage(arena, p_name, weapon, tot_damage, killed_objects) -- TODO killed_objects -> hit_objects = {[1] = {type, name, HP_prev, HP_left, dead = bool}}
end



function weapons_lib.deactivate_zoom(player, weapon)
  --TODO: rimuovere HUD zoom armi
  player:set_fov(0, nil, 0.1)

  local p_name = player:get_player_name()
  local p_meta = player:get_meta()

  p_meta:set_int("wl_zooming", 0)

  --[[TODO on_zoom? O forse ancora meglio on_speed_check, da utilizzare in qualsiasi campo.
  -- Che se ritorna ritorna true è SPEED
  local arena = arena_lib.get_arena_by_player(p_name)

  if arena and arena.players[p_name].stamina > 0 and ecc]]

  if (weapon.can_alter_speed and not weapon.can_alter_speed(player))
     and p_meta:get_int("wl_weapon_state") == 0
     and p_meta:get_int("wl_is_speed_locked") == 0 then
    speed_up(player, weapon.mod)
  end
end





----------------------------------------------
--------------------UTILS---------------------
----------------------------------------------

function weapons_lib.is_weapon(i_name)
  return minetest.registered_nodes[i_name] and minetest.registered_nodes[i_name]._is_weapon_wl
end




----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function register_action(w_name, action, key)
  if not action then return end

  action.key = key

  if action.type == "raycast" or action.type == "bullet" then
    action.ammo_per_use = action.ammo_per_use or 1
    action.delay = action.delay or 0.5
    action.loading_time = action.loading_time or 0

    if action.type == "raycast" then
      action.fire_spread = action.fire_spread or 0
    elseif action.type == "bullet" then
      assert(action.bullet, "Missing bullet in bullet action type")
      weapons_lib.register_bullet(w_name, action)
    end

  elseif action.type == "punch" then
    assert(action.continuous_fire == nil, "Punch actions can't have continuous fire")
  end

  if action.sound then
    local sound = action.sound
    audio_lib.register_sound("sfx", sound.track, sound.description, sound.params)
  end

  return action
end



function calc_action(weapon, action_id, player, pointed_thing)
  local is_holdable = ((action_id == 1 and weapon.action1_hold) or (action_id == 2 and weapon.action2_hold)) and true
  local in_the_air = weapon.weapon_type == "melee" and weapons_lib.is_in_the_air(player)
  local action, held_action

  if not in_the_air and is_holdable then
    action = action_id == 1 and weapon.action1_hold or weapon.action2_hold
    held_action = true
  else
    if action_id == 1 then
      action = (in_the_air and weapon.action1_air) and weapon.action1_air or weapon.action1
    else
      action = (in_the_air and weapon.action2_air) and weapon.action2_air or weapon.action2
    end
  end

  if not action or not can_use_weapon(player, weapon, action) then return end

  set_attack_stance(player, weapon, action)

  -- TODO: funzione per tempo d'attesa, prob sotto va poi messo in funzione a parte

  if action.attack_on_release then
    local held_key = action_id == 1 and "LMB" or "RMB"

    player:get_meta():set_int("wl_weapon_state", 2)
    wait_for_charged_action(weapon, action, held_key, player, action.load_time, 0)
  elseif held_action then
    local held_key = action_id == 1 and "LMB" or "RMB"

    player:get_meta():set_int("wl_weapon_state", 2)
    wait_for_held_action(weapon, action, held_key, player, 0.3)
  else
    run_action(weapon, action, player, pointed_thing)
  end
end



function wait_for_held_action(weapon, action, held_key, player, countdown)
  minetest.after(0.1, function()
    if not can_use_weapon(player, weapon, action, true) then return end

    if player:get_player_control()[held_key] then
      if countdown <= 0 then
        run_action(weapon, action, player)
      else
        countdown = countdown - 0.1
        wait_for_held_action(weapon, action, held_key, player, countdown)
      end
    else
      local action = held_key == "LMB" and weapon.action1 or weapon.action2
      run_action(weapon, action, player)
    end
  end)
end



function wait_for_charged_action(weapon, action, held_key, player, load_time, time)
  minetest.after(0.1, function()
    if not can_use_weapon(player, weapon, action, nil, true) then return end

    if player:get_player_control()[held_key] then
      if load_time > time then
        time = time + 0.1
      end

      wait_for_charged_action(weapon, action, held_key, player, load_time, time)
    else
      run_action(weapon, action, player)
    end
  end)
end



function can_use_weapon(player, weapon, action, held, charged)
  local p_name = player:get_player_name()
  local p_meta = player:get_meta()

  if action.type == "zoom" then
    return p_meta:get_int("wl_weapon_state") ~= 4
  end

  if player:get_hp() <= 0 or (weapon.can_use_weapon and not weapon.can_use_weapon(player, action)) then
    return end

  if held then
    if p_meta:get_int("wl_weapon_state") > 2 then return end

  elseif charged then
    if p_meta:get_int("wl_weapon_state") > 2 then return end

  else
    local w_magazine = p_data[p_name].magazine[weapon.name]

    if p_meta:get_int("wl_weapon_state") ~= 0 or
     (weapon.magazine and w_magazine ~= -1 and (w_magazine == 0 or action.ammo_per_use > w_magazine)) then
      return end
  end

  return true
end



function set_attack_stance(player, weapon, action)
  local p_meta = player:get_meta()
  local p_name = player:get_player_name()

  if p_meta:get_int("wl_immunity") == 1 then
    p_meta:set_int("wl_immunity", 0)
  end

  if slow_down_func[p_name] then
    slow_down_func[p_name]:cancel()
  end

  if p_meta:get_int("wl_is_speed_locked") == 0 then
    if action.physics_override then
      if action.physics_override == "FREEZE" then
        local p_pos = player:get_pos()
        local p_y = player:get_look_horizontal()
        local dummy = minetest.add_entity(p_pos, "block_league:dummy")
        player:set_attach(dummy, "", {x=0,y=-5,z=0}, {x=0, y=-math.deg(p_y), z=0})
      else
        player:set_physics_override(action.physics_override)
      end

      p_meta:set_int("wl_is_speed_locked", 1)
    else
      if not weapon._slow_down_user or (weapon.can_alter_speed and not weapon.can_alter_speed(player)) then return end

      p_data[p_name].speed_before_shooting = player:get_physics_override().speed
      slow_down(player, weapon.mod)
    end
  end
end



function slow_down(player, mod)
  minetest.chat_send_all("Velocità prima = " .. player:get_physics_override().speed)
  player:set_physics_override({ speed = player:get_physics_override().speed * weapons_lib.mods[mod].SHOOT_SPEED_MULTIPLIER })
  minetest.chat_send_all("Velocità ora = " .. player:get_physics_override().speed)
end



function speed_up(player, mod)
  minetest.chat_send_all("Velocità prima = " .. player:get_physics_override().speed)
  player:set_physics_override({ speed = player:get_physics_override().speed / weapons_lib.mods[mod].SHOOT_SPEED_MULTIPLIER })
  minetest.chat_send_all("Velocità ora = " .. player:get_physics_override().speed)
end



function run_action(weapon, action, player, pointed_thing)
  if action.type == "raycast" or action.type == "bullet" or action.type == "custom" then
    player:get_meta():set_int("wl_weapon_state", 2)
    attack_loop(weapon, action, player)

  elseif action.type == "punch" then
    player:get_meta():set_int("wl_weapon_state", 2)
    attack_loop(weapon, action, player, pointed_thing)

  elseif action.type == "zoom" then
    weapon_zoom(weapon, action, player)

  elseif action.type == "install" then
    player:get_meta():set_int("wl_weapon_state", 2)
    -- TODO

  elseif action.type == "parry" then
    -- player:get_meta():set_int("wl_weapon_state", 5)
  end
end


function attack_loop(weapon, action, player, pointed_thing)
  local p_name = player:get_player_name()

  --block_league.sound_play(action.sound, p_name) TODO su arena_lib
  if action.sound then
    audio_lib.play_sfx(action.sound.track, {to_player = p_name})
  end

  if action.type == "punch" then
    attack_melee(player, weapon, action, pointed_thing)
  elseif action.type == "custom" then
    action.on_use(player, weapon, action)
  else
    if weapon.magazine ~= -1 then
      decrease_magazine(player, weapon, action.ammo_per_use)
    end

    if action.type == "raycast" then
      attack_hitscan(player, weapon, action)
    elseif action.type == "bullet" then
      attack_bullet(player, action)
    end

  end

  -- interrompo lo sparo se non è un'arma a fuoco continuo
  if not action.continuous_fire then
    attack_end(player, weapon, action.delay)

  else
    minetest.after(action.delay, function()
      local w_magazine = p_data[p_name].magazine[weapon.name]

      if player:get_player_control()[action.key]
        and player:get_meta():get_int("wl_weapon_state") == 2
        and (weapon.magazine and (weapon.magazine == -1 or (w_magazine > 0 and action.ammo_per_use <= w_magazine))) then -- TODO: on_continuous_fire?
        attack_loop(weapon, action, player)
      else
        attack_end(player, weapon, action.delay)
      end
    end)
  end
end



function decrease_magazine(player, weapon, amount)
  local p_name = player:get_player_name()
  local w_name = weapon.name
  local mag = p_data[p_name].magazine

  mag[w_name] = mag[w_name] - amount

  -- automatically reload if the magazine is now empty
  if mag[w_name] == 0 then
    weapon_reload(player, weapon)
  else

    -- TODO: richiamo on_use? on_decrease_magazine?
    -- block_league.HUD_weapons_update(arena, p_name, w_name)
    return true
  end
end



function attack_hitscan(user, weapon, action)
  local pointed_objects = weapons_lib.get_pointed_objects(user, action.range, action.pierce) -- TODO: considerare raggio illimitato con -1?

  if action.trail then
    local dir = user:get_look_dir()
    local pos_head = vector.add(vector.add(user:get_pos(), vector.new(0,1.475,0)), dir)
    draw_particles(action.trail, dir, pos_head, action.range, action.pierce)
  end

  if pointed_objects then
    weapons_lib.apply_damage(user, pointed_objects, weapon, action)
  end
end



function attack_melee(user, weapon, action, pointed_thing)
  if pointed_thing.type ~= "object" then return end

  local object = pointed_thing.ref
  -- TEMP: non posso calcolare il colpo alla testa perché i `pointed_thing` nativi
  -- non ritornano `intersection_point`. Vedasi https://github.com/minetest/minetest/issues/13734
  --local critical = (pointed_thing.intersection_point.y - object:get_pos().y) > 1.275 and true
  local critical = false
  local target = {{object = object, critical = critical}}

  weapons_lib.apply_damage(user, target, weapon, action)
end



function attack_bullet(user, action)
  local pos = user:get_pos()
  local pos_head = {x = pos.x, y = pos.y + user:get_properties().eye_height, z = pos.z}
  local bullet_name = action.bullet.name .. '_entity'
  local bullet = minetest.add_entity(vector.add(pos_head, user:get_look_dir()), bullet_name, user:get_player_name())

  -- TODO: non si può fare su on_activate quello che segue?
  local bullet_ent = bullet:get_luaentity()
  bullet_ent._action = table.copy(action)

  local speed = bullet_ent._speed
  local dir = user:get_look_dir()

  bullet:set_velocity({
    x=(dir.x * speed),
    y=(dir.y * speed),
    z=(dir.z * speed),
  })

  local yaw = user:get_look_horizontal()
  local pitch = user:get_look_vertical()
  local rotation = ({x = -pitch, y = yaw, z = 0})

  bullet:set_rotation(rotation)
end



function attack_end(player, weapon, delay)
  local p_name = player:get_player_name()
  local p_meta = player:get_meta()

  if p_meta:get_int("wl_weapon_state") == 4 then return end

  p_meta:set_int("wl_weapon_state", 3)

  local w_name = weapon.name

  -- se sono armi bianche, aggiorno l'HUD qui che segnala che son state usate
  if not weapon.magazine then
    weapons_lib.HUD_crosshair_update(p_name, w_name, true)
  end

  --[[ TODO: on_attack_end?
  local arena = arena_lib.get_arena_by_player(p_name)
  if not weapon.magazine then
    block_league.HUD_weapons_update(arena, p_name, w_name, true)
  end
  ]]

  -- finisce attesa e ripristina eventuale fisica personalizzata
  minetest.after(delay, function()
    if p_meta:get_int("wl_weapon_state") ~= 4 then
      p_meta:set_int("wl_weapon_state", 0)
    end

    -- se ha la fisica personalizzata, ripristinala
    if p_meta:get_int("wl_is_speed_locked") == 1 then
      p_meta:set_int("wl_is_speed_locked", 0)

      if player:get_attach() then
        player:get_attach():remove()

      else
        player:set_physics_override(block_league.PHYSICS) -- TODO

        if weapon._slow_down_user and (not weapon.can_alter_speed or weapon.can_alter_speed(player)) then
          if p_meta:get_int("wl_weapon_state") ~= 0
            or player:get_fov() ~= 0 then
            slow_down(player, weapon.mod)
          end
        end
      end

    -- TEMP: se `delay` è 0.5, c'è il rischio che la funzione sotto venga chiamata
    -- prima di questa. Serve https://github.com/minetest/minetest/issues/13477. Al momento uso controllo speed_before_shooting
    elseif player:get_physics_override().speed ~= p_data[p_name].speed_before_shooting
       and weapon._slow_down_user
       and (not weapon.can_alter_speed or weapon.can_alter_speed(player))
       and p_meta:get_int("wl_weapon_state") == 0
       and player:get_fov() == 0 then

      if slow_down_func[p_name] then
        slow_down_func[p_name]:cancel()
      end

      speed_up(player, weapon.mod)
    end

    -- ripristino colore HUD per le armi bianche (faccio qui per non aver un terzo after più in alto)
    if not weapon.magazine then
      local curr_weap = p_data[p_name].current_weapon
      --block_league.HUD_weapons_update(arena, p_name, w_name, false) TODO
      weapons_lib.HUD_crosshair_update(p_name, curr_weap, false)
    end
  end)

  -- ripristina velocità dopo 0.5 secondi
  slow_down_func[p_name] = minetest.after(0.5, function()
    if not weapon._slow_down_user or (weapon.can_alter_speed and not weapon.can_alter_speed(player)) then return end

    if p_meta:get_int("wl_weapon_state") ~= 0
      or p_meta:get_int("wl_is_speed_locked") == 1
      or player:get_fov() ~= 0
      then return end

    speed_up(player, weapon.mod)
  end)
end



function weapon_zoom(weapon, action, player)
  local p_meta = player:get_meta()

  if player:get_fov() ~= action.fov then
    player:set_fov(action.fov, nil, 0.1)
    p_meta:set_int("wl_zooming", 1)
    -- TODO: applica texture, riproduci suono
  else
    weapons_lib.deactivate_zoom(player, weapon)
  end
end



function weapon_reload(player, weapon)
  local w_name = weapon.name
  local p_name = player:get_player_name()
  local p_meta = player:get_meta()

  if player:get_hp() <= 0
     or weapon.weapon_type == "melee" or not weapon.magazine
     or weapon.magazine <= 0 or p_meta:get_int("wl_weapon_state") == 4
     or p_data[p_name].magazine[w_name] == weapon.magazine
    then return end

  -- block_league.sound_play(weapon.sound_reload, p_name) -- TODO su arena_lib
  if weapon.sound_reload then
    audio_lib.play_sfx(weapon.sound_reload.track, {to_player = p_name})
  end

  -- TODO capire perché c'è questo controllo
  local to_slow_down = p_meta:get_int("wl_weapon_state") == 0

  p_meta:set_int("wl_weapon_state", 4)

  -- rimuovo eventuale zoom
  if weapon.action2 and weapon.action2.type == "zoom" and player:get_fov() == weapon.action2.fov then
    weapons_lib.deactivate_zoom(player, weapon)
  end

  if weapon._slow_down_user
     and (not weapon.can_alter_speed or weapon.can_alter_speed(player))
     and to_slow_down
     and p_meta:get_int("wl_is_speed_locked") == 0 then
    slow_down(player, weapon.mod)
  end

  weapons_lib.HUD_crosshair_update(p_name, w_name, true)

  -- TODO on_reload
  --block_league.HUD_weapons_update(arena, p_name, w_name, true)

  minetest.after(weapon.reload_time, function()
    p_meta:set_int("wl_weapon_state", 0)

    -- TODO: controlla se ha ancora l'arma nell'inventario e se è ancora vivə

    if weapon._slow_down_user and (not weapon.can_alter_speed or weapon.can_alter_speed(player)) then
      local mod = weapon.mod
      if p_meta:get_int("wl_is_speed_locked") then
        speed_up(player, mod)
      else
        slow_down(player, mod)
      end
    end

    local curr_weap = p_data[p_name].current_weapon

    p_data[p_name].magazine[w_name] = weapon.magazine
    weapons_lib.HUD_crosshair_update(p_name, curr_weap, false)

    --[[ TODO: on_reload_end
    block_league.HUD_weapons_update(arena, p_name, w_name, false)]]
  end)
end



function draw_particles(particle, dir, origin, range, pierce)
  local check_coll = not pierce

  minetest.add_particlespawner({
    amount = particle.amount,
    time = 0.3,   -- TODO: meglio funzione che approssima distanza? Time era 0.3, min/max erano impact_dist/(range * 1.5)
    pos = vector.new(origin),
    vel = vector.multiply(dir, range),
    size = particle.size or 1,
    collisiondetection = check_coll,
    collision_removal = check_coll,
    texture = particle.image
  })
end

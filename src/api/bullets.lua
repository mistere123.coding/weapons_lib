local function generate_entity() end
local function check_for_collisions() end
local function bullet_explode() end
local function spawn_particles_sphere() end



function weapons_lib.register_bullet(w_name, action)
  local bullet = action.bullet
  local bullet_entity = generate_entity(w_name, bullet, action)

  minetest.register_entity(bullet.name .. "_entity", bullet_entity)

  return bullet_entity
end





----------------------------------------------
---------------FUNZIONI LOCALI----------------
----------------------------------------------

function generate_entity(w_name, def, action)
  local bullet = {
    initial_properties = {
      name = def.name,
      visual = def.mesh and "mesh" or "item",
      mesh = def.mesh,
      visual_size = def.visual_size,
      textures = def.textures,
      collisionbox = def.collisionbox,

      physical = true,
      collide_with_objects = true,
    },

    _p_name = "", -- chi spara
    _action = action,
    _w_name = w_name,
    _damage = action.damage,
    _speed = def.speed,
    _bounciness = def.bounciness,
    _lifetime = def.lifetime,
    _curr_lifetime = 0,

    _explosion = def.explosion,
    _remove_on_contact = def.remove_on_contact,

    _bullet_trail = def.trail,
    _spawn_particle_time = 0,

    _gravity = def.gravity,

    _on_destroy = def.on_destroy,

    _is_bullet = true,
    _is_sliding = false, -- per capire se si sta muovendo su superfici piane (granate)
  }

  function bullet:_destroy()
    if self._explosion then
      spawn_particles_sphere(self.object:get_pos(), self._explosion.texture)
      bullet_explode(self)
    end

    self.object:remove()
  end



  -- Ottiene gli staticdata ogni 18 secondi circa
  function bullet:get_staticdata(self)
    if self == nil or self._p_name == nil then return end
    return self._p_name
  end



  -- quando si istanzia un'entità
  function bullet:on_activate(staticdata)
    if staticdata ~= "" and staticdata ~= nil then
      self._p_name = staticdata -- nome utente come staticdata
      self.object:set_armor_groups({immortal = 1}) -- lo imposta come immortale
    else -- se non ci sono gli staticdata necessari allora rimuove l'entità
      self.object:remove()
      return
    end
  end



  function bullet:on_step(dtime, moveresult)
    self._curr_lifetime = self._curr_lifetime + dtime

    if self._curr_lifetime >= self._lifetime then
      -- ESPLODE
      self:_destroy()
      return
    end

    local obj = self.object
    local velocity = obj:get_velocity()
    local pos = obj:get_pos()

    -- Controlla che il timer per mostrare le particelle che tracciano la granata sia superiore al valore definito e che esista una definizione delle particelle da creare
    if self._bullet_trail and self._spawn_particle_time >= self._bullet_trail.interval then
      self._spawn_particle_time = 0
      -- Aggiunge le particelle di tracciamento
      minetest.add_particle({
        pos = obj:get_pos(),
        velocity = vector.divide(velocity, 2),
        acceleration = vector.divide(obj:get_acceleration(), -5),
        expirationtime = self._bullet_trail.life,
        size = self._bullet_trail.size,
        texture = self._bullet_trail.image
      })

    -- Controlla che il timer per mostrare le particelle che tracciano la granata sia inferiore al valore definito e che esista una definizione delle particelle da creare
    elseif self._bullet_trail and self._spawn_particle_time < self._bullet_trail.interval then
      self._spawn_particle_time = self._spawn_particle_time + 1
    end

    if moveresult.collides then
      local collisions = moveresult.collisions

      if self._remove_on_contact then
        check_for_collisions(self, collisions, true)

      else
        if not vector.equals(collisions[1].old_velocity, velocity) and vector.distance(collisions[1].old_velocity, velocity) > 4 then
          if math.abs(collisions[1].old_velocity.x - velocity.x) > 5 then -- Controlla se c'è stata una grande riduzione di velocità
            velocity.x = collisions[1].old_velocity.x * (self._gravity and -0.5 or -1) -- Inverte la velocità e la riduce
          end

          if math.abs(collisions[1].old_velocity.y - velocity.y) > 5 then -- Controlla se c'è stata una grande riduzione di velocità
            velocity.y = collisions[1].old_velocity.y * (self._gravity and -0.3 or -1)  -- Inverte la velocità e la riduce
          end

          if math.abs(collisions[1].old_velocity.z - velocity.z) > 5 then -- Controlla se c'è stata una grande riduzione di velocità
            velocity.z = collisions[1].old_velocity.z * (self._gravity and -0.5 or -1)  -- Inverte la velocità e la riduce
          end

          obj:set_velocity(velocity)
        end

        if self._gravity then
          if not self._is_sliding and velocity.y == 0 then -- Controlla se la granata sta scivolando
            self._is_sliding = true -- Attiva l'attrito
          elseif self._is_sliding and velocity.y ~= 0 then
            self._is_sliding = false -- Non influisce sull'attrito
          end

          if self._is_sliding then -- Sta scivolando?
            if vector.distance(vector.new(), velocity) <= 1 and not vector.equals(velocity, vector.new()) then -- Se la granata si muove a malapena
              obj:set_velocity(vector.new(0, -9.8, 0)) -- Si assicura sia ferma
              obj:set_acceleration(vector.new())
            end
          end
        end
      end
    end

    if self._gravity then
      local direction = vector.normalize(velocity)
      local node = minetest.get_node(pos)
      local speed = vector.length(velocity)
      local yaw = minetest.dir_to_yaw(direction)
      local pitch = math.acos(velocity.y/speed) - math.pi/3
      -- Controlla che il pitch sia un numero
      if tostring(pitch) ~= 'nan' then
        obj:set_rotation({x = 0, y = yaw + math.pi/2, z = pitch}) -- Imposta la rotazione
      end

      local drag = self._is_sliding and math.max(minetest.registered_nodes[node.name].liquid_viscosity, 0.1) or 0 -- Ottiene l'attrito generato dal liquido che attraversa
      local acceleration = vector.multiply(velocity, -drag)

      -- Controlla che l'accelerazione sia un numero
      if tostring(acceleration) ~= 'nan' then
				obj:set_acceleration(acceleration) -- Imposta l'accelerazione
      end
    end
  end

  return bullet
end



function check_for_collisions(bullet, collisions, remove_on_contact)
  local has_hit = false
  for k, coll in pairs(collisions) do
    if coll.object then
      local obj = coll.obj
      if obj:is_player() then
        -- se non colpisco me stessə
        if obj:get_player_name() ~= bullet._p_name then
          weapons_lib.apply_damage(minetest.get_player_by_name(bullet._p_name), {object = obj}, minetest.registered_nodes[bullet._w_name], bullet._action)
          has_hit = true

        -- sennò..
        else
          if bullet._lifetime < (15 / bullet._speed) then -- TODO: controlla, mi sa di cagata
            obj:set_velocity({
              x=(coll.old_velocity.x),
              y=(coll.old_velocity.y),
              z=(coll.old_velocity.z),
            })
          end
        end

      elseif obj:get_luaentity() then
        local entity = obj:get_luaentity()
        -- se collide con un altro proiettile, esplodono entrambi
        if entity._is_bullet then
          has_hit = true
          entity:_destroy()
        end
      end
    elseif coll.type == "node" then
      has_hit = true
    end
  end

  if has_hit and remove_on_contact then
    bullet:_destroy()
    return
  end
end



function bullet_explode(self)
  local explosion = self._explosion
  local range = explosion.range
  local dmg = explosion.damage
  local origin = self.object:get_pos()
  local p_name = self._p_name

  if origin == nil or origin.x == nil or origin.y == nil or origin.z == nil then return end

  local objs = minetest.env:get_objects_inside_radius(origin, range)
  local targets = {}
  local entities = {}

  -- Se ho colpito qualcosa
  if objs then
    for _, obj in ipairs(objs) do
      if obj:is_player() then
        -- TODO: tutte queste variabili non servono dato che non viene passato il danno,
        -- tuttavia va effettivamente calcolato a seconda della distanza dal punto d'impatto
        local p_pos = obj:get_pos()
        local lenx = math.abs(p_pos.x - origin.x)
        local leny = math.abs(p_pos.y - origin.y)
        local lenz = math.abs(p_pos.z - origin.z)
        local hypot = math.sqrt((lenx * lenx) + (lenz * lenz))
        local dist = math.sqrt((hypot * hypot) + (leny * leny))
        local damage = dmg - (dmg * dist / range)

        -- TODO: fai che, in apply_damage, se c'è fuoco amico e ci si colpisce da solɜ ci si infligge 1/5 danno
        table.insert(targets, {object=obj})

      elseif obj ~= self.object and obj:get_luaentity() then
        local entity = obj:get_luaentity()
        table.insert(entities, entity)

        if entity.initial_properties and entity.initial_properties.collide_with_objects and not entity._is_bullet then
          table.insert(targets, {object=obj})
        end
      end
    end

    weapons_lib.apply_damage(minetest.get_player_by_name(p_name), targets, minetest.registered_nodes[self._w_name], self._action)
  end

  if #entities == 0 then return end

  self.object:remove()
  for _,entity in pairs(entities) do -- rimuovi proiettili coinvolti nell'esplosione
    if entity._is_bullet then
      entity:_destroy()
    end
  end
end



function spawn_particles_sphere(pos, texture)
  if not pos then return end

  minetest.add_particlespawner({
    amount = 80,
    time = 0.1,
    pos = pos,
    vel = {min = vector.new(-4, -4, -4), max = vector.new(4, 4, 4)},
    acc = {min = vector.new(0, 0.4, 0),  max = vector.new(0, -0.8, 0)},
    exptime = 0.5,
    size = {min = 1, max = 5},
    collisiondetection = false,
    vertical = false,
    texture = texture,
  })
end
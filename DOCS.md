(WIP)

## Registering a mod
In order to register any weapon, you need to register your mod first via:
```lua
weapons_lib.register_mod("mod_name", rules)
```

This allows you to define the general rules of your weapons. To do that, it uses a table which can contain the following fields (if you don't want to override anything, just declare an empty table):
 * `RELOAD_KEY`: (string) the key used to reload weapons that support such function. By default `"Q"`
	* **BEWARE**: right now, due to MT limitations (see [here](https://github.com/minetest/minetest/issues/13954)), it can't be anything but `"Q"`, which also means that weapons can't be dropped. Changing this value will prevent people from reloading, but it'll also allow them to drop weapons
 * `SHOOT_SPEED_MULTIPLIER`: (float) how much players are slowed down when using a weapon that features slowness. Default is `0.66`

## Weapon structure
* `description`: (string) the name of the weapon,
* `profile_description`: (string) the description shown in the profile GUI
* `mesh`: as in the Minetest API
* `tiles`: ^
* `wield_scale`: ^
* `inventory_image`: ^
* `weapon_type`: (string) can be `"ranged"`, `"melee"` or `"support"`. A ranged weapon can have melee functions, but not viceversa. Not sure if I'll maintain "support", which is currently not implemented
* `range`: (int, melee only) the range of the weapon. `4` by default
* `magazine`: (int, no melee) how many bullets the magazine contains; if `-1`, it's infinite
* `reload_time`: (int, no melee) how much time it takes to reload the weapon
* `sound_reload`: (table, no melee) the sound the weapon does when reloading. Format `{track = "name", description = "for accessibility", <params = {MT sound table parameters}>}`
* `crosshair`: (string) the image of the crosshair
* `slow_down_user`: (boolean) whether the player should be slowed down when attacking and reloading
* `can_use_weapon`: (function(player, action)) additional checks about whether the weapon can shoot. Return `true` to shoot
* `can_alter_speed`: (function(player)) whether the weapon should either slow down or speed up the player, if `slow_down_user` option is enabled. Not returning `true` will ignore such alterations
* `action1`: (table) action on left click
* `action2`: (table) action on right click
* `action1_hold`: (table, melee only) action on left click if kept pressed for 0.3s. NOT YET IMPLEMENTED
* `action2_hold`: (table, melee only) action on right click if kept pressed for 0.3s. NOT YET IMPLEMENTED
* `action1_air`: (table, melee only) action on left click whilst in the air
* `action2_air`: (table, melee only) action on right click whilst in the air

### Bullet structure
* `remove_on_contact`: (boolean) whether the bullet should cause an explosion when hitting something. It needs `explosion`

### Actions structure
-- TODO suddividi tra parametri condivisi e quelli specifici per tipo (con sottosezioni)
* `type`: (string) can be `"raycast"`, `"bullet"`, `"zoom"`, `"install"`, `"melee"`, `"parry"` or `"custom"`
* `description`: (string) what the action does. Displayed in formspecs
* `damage`: (float) how much damage deals
* `range`: (float, no punch) range of the weapon
* `delay`: (float) how much time it should pass between being able to rerun the action
* `loading_time`: (float) how much time before actually running the action. NOT YET IMPLEMENTED
* `knockback`: (int) how much knockback the weapon should have
* `ammo_per_use`: (int, no punch) how much ammo is needed to run the action
* `physics_override`: (table or string) how the player physics should change when running the action. It takes either a Minetest phyiscs table or the string "FREEZE", which will block the player. Physics is restored when the action ends. NOT YET IMPLEMENTED (better said, it'll crash)
* `decrease_damage_with_distance`: (bool) whether damage should decrease as the distance from the target increases. Default is `false`. TODO: metti nella sezione solo raycast
* `continuous_fire`: (bool, no punch) whether it should keep firing when holding down the action key (waiting `delay` seconds between a shot and another). Default is `false`
* `attack_on_release`: (bool) whether it should attack when the action key is released. NOT YET IMPLEMENTED
* `pierce`: (bool) whether the hit should stop on the first person or continue. Default is `false`
* `sound`: (table) the sound to play when the action is run. Same structure as in `sound_reload`
* `trail`: (table) the trail the action should leave. Fields are:
  * `image`: (string) the particle to spawn
  * `amount`: (int) how many particles to draw along the line
* `on_use`: (function(player, weapon, action, pointed_thing), custom only) the behaviour of the custom action

## Notes
* `*_hold` actions are not implemented due to wielditem animations not being customisable. People would be confused. See: https://github.com/minetest/minetest/issues/2811
* same applies for `loading_time`
* `attack_on_release` needs https://github.com/minetest/minetest/issues/13581 to run properly (I only need it for the pixelgun at the moment)
* `bl_weapon_state` metadata communicates the state of the weapon:
  * `0`: free
  * `1`: loading, NOT YET IMPLEMENTED
  * `2`: shooting
  * `3`: recovering
  * `4`: reloading
  * `5`: parrying, NOT YET IMPLEMENTED

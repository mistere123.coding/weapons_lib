local version = "0.0.1-dev"
local modpath = minetest.get_modpath("weapons_lib")
local srcpath = modpath .. "/src"

weapons_lib = {}
weapons_lib.mods = {}



-- general
dofile(srcpath .. "/api/bullets.lua")
dofile(srcpath .. "/api/mods.lua")
dofile(srcpath .. "/api/weapons.lua")
dofile(srcpath .. "/player_manager.lua")
dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/HUD/hud_crosshair.lua")
dofile(srcpath .. "/tests/test1.lua")
dofile(srcpath .. "/tests/test2.lua")
dofile(srcpath .. "/tests/test3.lua")

minetest.log("action", "[WEAPONS_LIB] Mod initialised, running version " .. version)

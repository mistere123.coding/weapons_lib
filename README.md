# Weapons_lib

Library for Minetest to easily register new weapons.  
  
WIP, use it at your own risk :D  

<a href="https://liberapay.com/Zughy/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support my work"/></a>  

### How to
Check the [DOCS](DOCS.md)

### Credits
Music under CC0
Pixel arts by me, all CC0 except for crosshairs (CC BY-SA 4.0)

